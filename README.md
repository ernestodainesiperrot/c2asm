# C to Assembly

Small script to convert C code into Assembly.

## How to use

### Linux

Make the shell script executable:

```shell
chmod +x c2asm.sh
```
Now you can run it:

```shell
./c2asm your_c_file.c
```

> Tip: you can add the script to your PATH to use it as a terminal command

